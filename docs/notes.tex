\documentclass[11pt]{article}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb, amsmath, physics}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\newcommand{\vc}[1]{\mathbf{#1}}
\newcommand{\mvc}[1]{\tilde{\mathbf{#1}}}
\newcommand{\uv}[1]{\hat{\mathbf{#1}}}
\newcommand{\lagr}{\mathcal{L}}

\title{Multiscale Machine Learning}
\author{Austin Gilbert}
\date{\today}                                           % Activate to display a given date or no date

\begin{document}
\maketitle

\section{Systems}
High fidelity system is based on 2 moment fluid equations with "linearized"
momentum equation defined on the domain $(x,t)\in[0,1]\times[0,T]$ with
T an unspecified value. Boundary conditions and initial values are part of
the parameter space being explored. 
\subsection{High-Fidelity (Microscopic)}
In the differential form (aka assuming local smoothness)
the system of equations is given by
\begin{align}
\partial_{t}n + \partial_{x}(nu) &= 0\\
\partial_{t}(nu) + \partial_{x}\left(nu^{2}\right) &= F[n,u]
\end{align}
The system can also be cast in terms of conserved variables $q_{1}(x,t)=n(x,t)$ 
and $q_{2}(x,t)=n(x,t)u(x,t)$:
\begin{align*}
\partial_{t}q_{1} + \partial_{x}q_{1} &= 0\\
\partial_{t}q_{2} + \partial_{x}\left(
\frac{q_{2}^{2}}{q_{1}}\right) &= F[q_{1},q_{2}]
\end{align*}
Alternation between the two forms will be clear 
throughout the paper. 
\subsection{Low-Fidelity (Macroscopic)}
From the behavior of these equations several properties can be deduced that 
suggest and constrain how numerical solutions of this problem can be 
obtained with machine learning.
\begin{list}{}{spacing}
	\item Conservation of correlation functions from conservation of
	variables->statistics can be centered (especially with periodic bcs).
	\item Time reversibility (depending on form of $F$) -> model should
	not destroy information but enable reversibility.
	\item Jump discontinuities->reduced order methods based on functional 
	sets defined over spatial domain are valid.  
\end{list}
\begin{align}
\partial_{t}q_{} + \partial_{x}(nu) &= 0\\

\end{align}
\subsection{Closures}
Five functional closures designed to mimic inert flow, distant action, 
ideal gas pressure, diffusion, relaxation, and reactions will be considered:
\begin{align}
f_{null}[n] &= 0\\
f_{ad}[n] &= -n(x)\partial_{x}\int\frac{n(x')}{|x-x'|}dx'\\
f_{press}[n] &= -\partial_{x}n\\
\end{align}

\subsection{Initial Conditions}
For expediency, only periodic domains will be initially considered, but this may change depending
on how things go. Several initial conditions will be tested in order to investigate the "memory"
of the system each closure has associated with it. Three initial conditions 
from a two parameter family $(N,\varepsilon)$ will be used.

\subsubsection{IC1: Universal Waves}
\[
n_{0}(x) = \sin(N\pi x) + 1.0
\]
\[
u_{0}(x) = \sin(N\pi x)
\]

\subsubsection{IC1: Uniform Small Velocity}
\[
n_{0}(x) = \sin(N\pi x) + 1.0
\]
\[
u_{0}(x) = \varepsilon
\]

\subsubsection{IC1: Uniform Small Density}
\[
n_{0}(x) = \varepsilon
\]
\[
u_{0}(x) = \sin(N\pi x)
\]

\section{Numerics}

\subsection{Rusanov Method}
For expediency\footnote{Also since the null closure is only weakly hyperbolic...},
centered artificial viscosity methods are used to solve the system. If 
$\vc{Q}^{n}_{i}$ represents the vector of average values of each conserved variable,
and $\vc{F}_{i-1/2}$ the flux vector at the left interface of the ith cell, the update equations follow
\[
\vc{F}_{i-1/2} = \frac{1}{2}\left[\vc{f}(\vc{Q}^{n}_{i-1})
+\vc{f}(\vc{Q}^{n}_{i})-\max_{\vc{Q}\in \left[\vc{Q}^{n}_{i-1},\vc{Q}^{n}_{i}\right]}\lambda(\vc{F}'(\vc{Q}))\right]
\]
\[
\vc{Q}^{n+1}_{i} = \vc{Q}^{n}_{i} + \frac{dt}{dx}\left[\vc{F}_{i-1/2}-\vc{F}_{i+1/2}\right]
\]


\section{Regression Theory}

\subsection{Linear Vector Auto-Regression}

\subsubsection{Preliminaries}
\paragraph*{}Define the vector of predicted variables at discrete time step $k$ ($t=k\cdot dt$) over a domain of $N_{x}$ cells by
\[
\vc{Y}^{k} = \begin{bmatrix}
m_{0} & m_{1} & \cdots & m_{N_{x}-2} & m_{N_{x}-1}
\end{bmatrix}^{T}
\]
where $m_{i}=u_{i}n_{i}$ represents the mean momentum density across cell $i$. Next
define the vector of known variables at the same time step over the same domain by
\[
\vc{X}^{k} = \begin{bmatrix}
n_{0} & n_{1} & \cdots & n_{N_{x}-2} & n_{N_{x}-1}
\end{bmatrix}^{T}
\] 
An element wise  time mean for an ordered tuple of vectors
\footnote{Notation here is intended for $\mathcal{T}$ to denote a
discrete time series of an object.} $\mathcal{T}(\vc{Q})=\{\vc{Q}^{i}|i\in\left[0,N_{t}\right]\}$ 
with $\vc{Q}^{i}\in\mathbb{R}^{N_{x}}$ is defined by 
\[
\mu_{j}(\mathcal{T}(\vc{Q})) = \frac{1}{N_{t}+1}\sum_{i=0}^{N_{t}}Q^{i}_{j}
\]
Again for clarity, superscripts are associated with time step and subscripts 
denote cell indices. Using the newly defined mean vector 
$\boldsymbol{\mu}(\mathcal{T}(\vc{Q}))$, define the 'centered' vector of
values at time step $k$ by
\[
\mvc{Q}^{k} = \vc{Q}^{k}-\boldsymbol{\mu}(\mathcal{T}(\vc{Q}))
\]
\paragraph*{}With these definitions, it is finally possible to set up
the scaffolding to generate our goal: a 2 parameter linear predictor 
defined by the  maximum lag, $l_{max}$, and maximum dependence width $w_{max}$
on either side of a given node. Both of these parameters are integers. Lag 
determines the number of terms in the predictor:
\[
\mvc{Y}^{k} = \sum_{l=1}^{l_{max}}A^{l}\begin{bmatrix}
\mvc{Y}^{k-l}\\ \mvc{X}^{k-l}
\end{bmatrix}
= \sum_{l=1}^{l_{max}}A^{l}\mvc{Z}^{k-l}
\] 
where $A^{l}\in\mathbb{R}^{N_{x}\times 2N_{x}}$. To understand the effect of
$w_{max}$, first define the periodic index function\footnote{
The symbol $\%$ refers to the integer modulus}
\[
\mathfrak{p}(i,l) \equiv (N_{x}+i+l)\% N_{x}
\]
In essence $\mathfrak{p}$ allows one to switch from indexing by column to 
indexing by column relative to the row value (i.e. from diagonal). Breaking
up the matrix $A^{l}$ into two matrices indicating the action of each variable
$\mvc{Y}$ and $\mvc{X}$ as
\[
A^{l} = \begin{bmatrix}
B^{l} & C^{l}
\end{bmatrix}
\]
then the parameter $w_{max}$ dictates that for $\abs{l}>w_{max}$,
\[
B^{l}_{i,\mathfrak{p}(i,l)} = 0
\]
\[
C^{l}_{i,\mathfrak{p}(il)} = 0
\]
Or, re-collapsing the matrix,
%%The second subscript in the second equality below
%%may be off by one depending on if we index from 0 or not.   
\[
A^{l}_{i,\mathfrak{p}(il)} = A^{l}_{i,\mathfrak{p}(il)+N_{x}}=0
\]

\subsubsection{Least Squares Solution}
Our loss function that is to be minimized is defined by
\footnote{The Hadamard product of two matrices of the same
shape is defined as $(A\circ B)_{ij}=A_{ij}B_{ij}$}
\begin{align*}
\lagr(A^{1},\ldots,A^{l_{max}}) &= 
\sum_{i=l_{max}}^{N_{t}}\norm{\mvc{Y}^{i}-\sum_{j=1}^{l_{max}}A^{j}
\mvc{Z}^{i-j}}^{2} + \sum_{k=1}^{l_{max}}\Lambda^{k}\circ A^{k}
\end{align*}
In the above, note that elements of $\Lambda^{k}$ are such that 
for $\abs{l}\leq w_{max}$,
\[
\Lambda^{k}_{i,\mathfrak{p}(i,l)} = 0
\]
and each $A^{i}\in \mathbb{R}^{N_{x}\times (2N_{x})}$. This can be 
further collapsed by defining $A\in\mathbb{R}^{N_{x}\times l_{max}(2N_{x})}$
as
\[
A = \begin{bmatrix}
A^{1}&\cdots&A^{l_{max}}
\end{bmatrix}
\]
and $\mvc{W}^{i}\in\mathbb{R}^{2l_{max}N_{x}}$ as
\[
\mvc{W}^{i} = \begin{bmatrix}
\tilde{Z}^{i-1}\\
\vdots\\
\tilde{Z}^{i-l_{max}}
\end{bmatrix}
\]
To find
\[
\lagr(A,\Lambda) = \sum_{i=l_{max}}^{N_{t}}\norm{\mvc{Y}^{i}-A\mvc{W}^{i}}^{2}
+ \Lambda\circ A
\]
Then let $\mvc{Y} = \begin{bmatrix}
\mvc{Y}^{l_{max}}&\cdots & \mvc{Y}^{N_{t}}
\end{bmatrix}$ and $\mvc{W}=\begin{bmatrix}
\mvc{W}^{l_{max}}&\cdots & \mvc{W}^{N_{t}}\end{bmatrix}$ to find
\[
\lagr(A) = \Tr\left[(\mvc{Y}-A\mvc{W})(\mvc{Y}-A\mvc{W})^{T}\right] + \Lambda\circ A
\]
taking the gradient yields
\[
\frac{\partial \lagr}{\partial A} = -2\mvc{Y}\mvc{W}^{T} + 2A\mvc{W}\mvc{W}^{T}
+\Lambda = 0
\]
\[
\frac{\partial \lagr}{\partial \Lambda} = A
\]
Yielding
\[
A = (\mvc{Y}\mvc{W}^{T}-\frac{1}{2}\Lambda)(\mvc{W}\mvc{W}^{T})^{-1}
\]
\end{document}  