"""Make plots of various sorts."""
import numpy as np
import glob
from plots import plot_vspacetime, plot_corrs, plot_space_stats
from params import solvers
from params import gNx, gtNt, gdt, gxlo, gxhi, gtio
from params import ics
from params import make_pplots, make_stplots, make_cplots, make_kplots,\
	 make_ssplots
from ics import gen_sol
import matplotlib.pyplot as plt

#all grid files
gfiles = glob.glob('data/*Grid.npz')

dx = (gxhi-gxlo)/gNx
xs = gxlo + np.linspace(0.5*dx, (gNx-0.5)*dx, num=gNx)
ts = np.linspace(0, gtNt*gdt, num=gtNt+1)

for fn in gfiles:
	ofb = fn.strip('Grid.npz')
	ofb = ofb.strip('data/')
	vals = np.load(fn)
	vlist = []
	vlabs = []
	for k in vals.keys():
		vlist.append(vals[k])
		vlabs.append(k)
	if make_stplots:
		#stp -> space time plot
		fn = '%s_stp.png' % ofb
		plot_vspacetime(xs, ts, vlist, vlabs, fn)
	if make_cplots:
		fn = '%s_Corr' % ofb
		plot_corrs(vlist, vlabs, fn)


#possibly useful at a later point. 
# these are per a timestep visualizations,
# also most useful when applied to all outputs
# of a common ic. 

# if make_pplots:
# 	#get max lims.
# 	nmax = 0.0
# 	umax = 0.0
# 	tn = 0.0
# 	tu = 0.0
# 	for k in sols.keys():
# 		tn = np.amax(sols[k][:,:,0])
# 		tu = np.amax(np.abs(sols[k][:,:,1]))
# 		if tu > umax:
# 			umax = tu
# 		if tn > nmax:
# 			nmax = tn
# 	for i in range(t.shape[0]):
# 		s = t.shape[0]-1-i
# 		plt.close('all')
# 		f, axs = plt.subplots(2, 1, sharex=True, figsize=(8,8))
# 		for k in sols.keys():
# 			axs[0].plot(x, sols[k][i,:,0], label=k)
# 			axs[1].plot(x, sols[k][i,:,1])
# 		axs[0].legend()
# 		axs[0].set_ylabel('n')
# 		axs[0].set_ylim(bottom=0.0, top=nmax)
# 		axs[1].set_ylabel('u')
# 		axs[1].set_xlabel('x')
# 		axs[1].set_ylim(bottom=-1*umax, top=umax)
# 		ft = '%s t=%f' % (title, t[s])
# 		f.suptitle(ft, fontsize=16)
# 		plt.savefig('%s_step%04d.png' % (title, s))

# if make_ssplots:
# 	#generate list of usols, nsols
# 	ns = []
# 	us = []
# 	labs = []
# 	for k in sols.keys():
# 		ns.append(sols[k][:,:,0])
# 		us.append(sols[k][:,:,1])
# 		labs.append(k)
# 	s1 = 'NDensity'
# 	s2 = 'Velocity'
# 	f1 = '%s_ndens_space_stats.png' % title
# 	f2 = '%s_vel_space_stats.png' % title
# 	plot_space_stats(ts, ns, labs, s1, f1)
# 	plot_space_stats(ts, us, labs, s2, f2)