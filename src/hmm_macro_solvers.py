"""'Traditional' hmm fvm solutions to isothermal euler equations
with unspecified eos."""
import numpy as np
from micro_solvers import PExRmSolver, PExNtmSolver

#uber-pedantic note: is it more reasonable to
#specify a domain and then set of equations,
#or set of equations and then a domain?

class PHmmRmSolver(PExRmSolver):
    """*P*eriodic Heterogeneous multiscale method (HMM) Russanov solver class.

    This is an object for solving the 1d isothermal Euler equations with
    unspecified  equation of state to first order. HMM designates the fact that
    the momentum equation is not updated every time the continuity equation is
    updated.
    
    Parameters
    ----------
    s : int
        Number of density updates to apply per a single momentum update. 
    Nx : int
        Number of spatial grid cells.
    Nt : int
        Number of time samples to store beyond given initial condition.
    dt : float
        Time interval between spatial solutions stored in `grid`.
    closure : {'Press', 'Ad', 'None'}
        How the right hand side of the momentum equation is fixed.
    ninit : function
        Function used to set initial number density values.
    uinit : function
        Function used to set initial velocity values.
    cfl : float, optional
        Value between 0 and 1.0 specifying the fraction of a cell length that
        can be traversed by waves per timestep. (default:0.7)
    xlo : float, optional
        Left most x-value of spatial domain. (default:0.0)
    xhi : float, optional
        Right most x-value of spatial domain. (default:1.0)
    io : {'None', 'All', 'Grid', 'Both'}, optional
        Specifies how to output data. 'All' writes out every single timestep,
        including subcycles. 'Grid' only writes out the final grid. For custom
        file names, add a 'fn' kwarg. 
    cvout : boolean, optional
        Determines if the conserved variables or independent fields are written
        to disk. 
        
    Attributes
    ----------
    o : int
        Solver order.
    s : int
        Number of density updates per momentum update.
    nsteps : int
        Total number of steps taken.
    io : str, {'None', 'All', 'Grid'}
        Output specification.
    closure : str, {'Press', 'Ad', 'None'}
        Closure being used on the system.
    base_fn : str
        Base file name for outputs.
    dx : float
        Cell widths.
    dt : float
        Temporal spacing of solutions in `grid`.
    grid : np.ndarray of float64
        Array for approximate solution, has shape ('Nt'+1, 'Nx')
    stmp : np.ndarray of float64
        Array for temporary storage of approximate solution, used in e.g.
        sub-cycling.
    phi : np.ndarray of complex64
        An array for a potential, only created if `closure` holds value of
        'Ad'.
    ftmp : np.ndarray of float64
        Array of approximate fluxes.
    """
    def __init__(self, s, Nx, Nt, dt, closure, ninit, uinit, cfl=0.7, 
                    xlo=0.0, xhi=1.0, io='None', cvout=True, **kwargs):
        self.s = s
        super().__init__(Nx, Nt, dt, closure, ninit, uinit, cfl=cfl, 
                            xlo=xlo, xhi=xhi, io=io, cvout=cvout, **kwargs)
        	
    def step(self, i):
        """Generate and store the approximate solution across the spatial 
		domain at time step i.
		
		Parameters
		----------
		i : int
			The index of the sub-array of `self.grid` to put new data in.
		"""
		#local t info. Could re-factor so dt is used in _find_dt,
        #decreasing mem use.
        tleft = self.dt
        dt = tleft
        p = 0.0
        while tleft != 0.0:
            self.nsteps += 1
            dt = self._find_dt(i, tleft)
            self._setf(i)
            self.grid[i,1:-1,:] = self.stmp[1:-1, :]
            if self.nsteps%self.s == 0:
                self.grid[i,1:-1,:] += dt*(self.ftmp[:-1,:]-self.ftmp[1:,:])/self.dx
            else:
                self.grid[i,1:-1,0] += dt*(self.ftmp[:-1,0]-self.ftmp[1:,0])/self.dx
			#update bc
            self.grid[i,0,:] = self.grid[i,-2,:]
            self.grid[i,-1,:] = self.grid[i,1,:]
			#store new result
            self.stmp[:,:] = self.grid[i,:,:]
            if self.closure == 'Ad':
                self._solve_splitop(i,dt)
            if self.io == 'All' or self.io=='Both':
                fn = '%s%06d' % (self.base_fn, self.nsteps)
                self._save_step(fn, dt, self.stmp[1:-1,:])
            tleft -= dt
            p = 1.0 - tleft/self.dt
            print("Subcycling. %f percent done with time step." % p)


class PHmmNtmSolver(PExNtmSolver):
    """*P*eriodic Heterogeneous multiscale method (HMM) NTM solver class.

    This is an object for solving the 1d isothermal Euler equations with
    unspecified  equation of state to second order. HMM designates the fact that
    the momentum equation is not updated every time the continuity equation is
    updated.
    
    Parameters
    ----------
    s : int
        Number of density updates to apply per a single momentum update. 
    o : int
        Order of solver being used. 
    Nx : int
        Number of spatial grid cells.
    Nt : int
        Number of time samples to store beyond given initial condition.
    dt : float
        Time step between the spatial solutions stored in `grid`.
    closure : {'Press', 'Ad', 'None'}
        How the right hand side of the momentum equation is fixed.
    ninit : function
        Function used to set initial number density values.
    uinit : function
        Function used to set initial velocity values.
    cfl : float, optional
        Value between 0 and 1.0 specifying the fraction of a cell length that
        can be traversed by waves per timestep. (default:0.7)
    xlo : float, optional
        Left most x-value of spatial domain. (default:0.0)
    xhi : float, optional
        Right most x-value of spatial domain. (default:1.0)
    io : {'None', 'All', 'Grid'}, optional
        Specifies how to output data. 'All' writes out every single timestep,
        including subcycles. 'Grid' only writes out the final grid. For custom
        file names, add a 'fn' kwarg. 
    cvout : boolean, optional
        Determines if the conserved variables or independent fields are written
        to disk. 

    Attributes
    ----------
    s : int
        Number of density updates to apply per a single momentum update.
    o : int
        Solver order.
    nsteps : int
        Total number of steps taken.
    io : str, {'None', 'All', 'Grid'}
        Output specification.
    closure : str, {'Press', 'Ad', 'None'}
        Closure being used on the system.
    base_fn : str
        Base file name for outputs.
    dx : float
        Cell widths.
    dt : float
        Temporal spacing of solutions in `grid`, initially 0.
    grid : np.ndarray of float64
        Array for approximate solution, has shape ('Nt'+1, 'Nx')
    stmp : np.ndarray of float64
        Array for temporary storage of approximate solution, used in e.g.
        sub-cycling.
    ftmp : np.ndarray of float64
        Array of approximate fluxes.
    """
    def __init__(self, s, Nx, Nt, dt, closure, ninit, uinit, cfl=0.7, 
                    xlo=0.0, xhi=1.0, io='None', cvout=True, **kwargs):
        self.s = s
        super().__init__(Nx, Nt, dt, closure, ninit, uinit, cfl=cfl, 
                        xlo=xlo, xhi=xhi, io=io, cvout=cvout, **kwargs)
    
    def step(self, i):
        """Generate and store the approximate solution across the spatial 
		domain at time step i.
		
		Parameters
		----------
		i : int
			The index of the sub-array of `self.grid` to put new data in.
		"""
		#local t info. Could re-factor so dt is used in _find_dt,
        #decreasing mem use.
        tleft = self.dt
        dt = tleft
        p = 0.0
        while tleft != 0.0:
            self.nsteps += 1
            dt = self._find_dt(i, tleft)
            #for ad do strang split
            if self.closure=='Ad':
                dt = 0.5*dt
            #ad closure split is hyperbolic-ad-hyperbolic
            #ssp rk2
            #first step (ad: step 1, operator 1)
            self._set_sig(i)
            self._setf1(i, dt)
            if self.nsteps%self.s == 0:
                self.stmp[2:-2,:] += (self.ftmp[:-1,:]-self.ftmp[1:,:]) 
            else:
                self.stmp[2:-2,0] += (self.ftmp[:-1,0]-self.ftmp[1:,0]) 
            #pbc
            self.stmp[:2,:] = self.stmp[-4:-2,:]
            self.stmp[-2:,:] = self.stmp[2:4,:]
            #step 2 (ad: step 2, operator 1)
            self._set_sig(i)
            self._setf2(i, dt)
            self.grid[i,2:-2,:] = self.stmp[2:-2,:]
            if self.nsteps%self.s == 0:
                self.grid[i,2:-2,:] += (self.ftmp[:-1,:]-self.ftmp[1:,:])
            else:
                self.grid[i,2:-2,0] += (self.ftmp[:-1,0]-self.ftmp[1:,0])
            #pbc
            self.grid[i,:2,:] = self.grid[i,-4:-2,:]
            self.grid[i,-2:,:] = self.grid[i,2:4,:]
			#store new result
            self.stmp[:,:] = self.grid[i,:,:]
            if self.closure == 'Ad':
                if self.nsteps%self.s == 0:
                    self._solve_splitop(i, 2.0*dt)
                #operator 1, step 1
                self._set_sig(i)
                self._setf1(i, dt)
                if self.nsteps%self.s == 0:
                    self.stmp[2:-2,:] += (self.ftmp[:-1,:]-self.ftmp[1:,:])
                else:
                    self.stmp[2:-2,0] += (self.ftmp[:-1,0]-self.ftmp[1:,0])
                #pbc
                self.stmp[:2,:] = self.stmp[-4:-2,:]
                self.stmp[-2:, :] = self.stmp[2:4,:]
                #operator 1, step 2
                self._set_sig(i)
                self._setf2(i, dt)
                self.grid[i, 2:-2,:] = self.stmp[2:-2,:]
                if self.nsteps%self.s == 0:
                    self.grid[i,2:-2,:] += (self.ftmp[:-1,:]-self.ftmp[1:,:])
                else:
                    self.grid[i,2:-2,0] += (self.ftmp[:-1,0]-self.ftmp[1:,0])
                #pbc
                self.grid[i,:2,:] = self.grid[i,-4:-2,:]
                self.grid[i,-2:,:] = self.grid[i,2:4,:]
                #store new result
                self.stmp[:,:] = self.grid[i,:,:]
                dt = 2.0*dt
            if self.io == 'All' or self.io=='Both':
                fn = '%s%06d' % (self.base_fn, self.nsteps)
                self._save_step(fn, dt, self.stmp[2:-2,:])
            tleft -= dt
            p = 1.0 - tleft/self.dt
            print("Subcycling. %f percent done with time step." % p)