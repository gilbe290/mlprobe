import numpy as np
from plots import plot_vspacetime, plot_corrs
from params import gNx, gNt, gdt, gcfl
from params import solvers
from params import ics
from ics import gen_sol
from var import generate_amat_err, generate_y, generate_z
import matplotlib.pyplot as plt



lmaxs = np.arange(1,33)
errs = {}
j = 0

for i in ics:
    sols = {}
    j = i
    s = 'Ic%d' % j
    for k in solvers.keys():
        xs, ts = gen_sol(sols, k, solvers[k], i, gNx, gNt, gdt, gcfl)
    for k in sols.keys():
        errs[k] = []
        y, ymu = generate_y(sols[k][:,:,1])
        z, zmu = generate_z(sols[k][:,:,1], sols[k][:,:,0])
        for j in lmaxs:
            print('On lmax = %d' % j)
            errs[k].append(generate_amat_err(y, z, j))
    plt.close('all')
    plt.figure(figsize=(10,10))
    for k in errs.keys():
        plt.semilogy(lmaxs, errs[k], label=k)
    plt.legend()
    plt.xlabel(r'$N_{lags}$')
    plt.ylabel(r'MSE')
    plt.title('Un-constrained Var MSE for %s' % s)
    fn = "%sMse.png" % s
    plt.savefig(fn)