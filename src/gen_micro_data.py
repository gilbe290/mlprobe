"""Iterates through all test cases specified in `params` and generates
data over a number of timesteps for each."""
import numpy as np
import glob
from params import micro_solvers, closures, ics, icsuf, gNx, gtNt, gdt, gcfl, gxlo,\
	gxhi,gtio,gtcvo,gtcvsuf,gnlags
from ics import gen_sol

def store_data(bfile, ofile, nlags, nx, cvo):
	"""Takes a set of outputs and writes them to a single file for 
	future use."""
	files = sorted(glob.glob(bfile))
	files = files[-1*(nlags):]
	ogrid = np.zeros((nlags, nx, 2), dtype=np.float64)
	ihi = nlags
	itmp = 0
	for i in range(len(files)):
		#ensures last file occurs first.
		itmp = ihi-i-1
		ars = np.load(files[i])
		ogrid[itmp,:,0] = ars['n']
		#ensure we always output velocity field for restart.
		#mainly because that is just how solvers are already set up.
		if cvo:
			ogrid[itmp, :, 1] = ars['nu']/ars['n']
		else:
			ogrid[itmp,:,1] = ars['u']
	np.savez(ofile, sgrid=ogrid)


for sk in solvers.keys():
	for c in closures:
		for i in ics:
			fns = 'data/%s%s%s%s' % (sk, c, icsuf[i], gtcvsuf)
			gen_sol(solvers[sk], gNx, gtNt, gdt, c, i, gcfl, gxlo, gxhi,
						gtio, gtcvo, fns)
			for lag in gnlags:
				bf = '%s*[0-9].npz' % fns
				ofn = 'data/Restart%d%s%s%s%s' % (lag, sk, c, icsuf[i], gtcvsuf)
				store_data(bf, ofn, lag, gNx, gtcvo)