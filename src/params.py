"""Parameters for all scripts in folder."""
from micro_solvers import PExRmSolver, PExNtmSolver
from hmm_macro_solvers import PHmmRmSolver, PHmmNtmSolver
from hvar_macro_solvers import PVarRmSolver, PVarNtmSolver


######################
## TOY HYDRO PARAMS ##
######################
#which solvers to use.keys serve as first part of output filenames,
#and are modified by case specific suffixes.
#solvers = {'Rm':PExRmSolver,'Ntm':PExNtmSolver}
micro_solvers = {'Ntm':PExNtmSolver}
#hmm solvers
hmm_solvers = {'HmmNtm':PHmmNtmSolver}
hmm_svals = [1, 2, 3, 4, 5]
#vsolvers = {'Rm':PVarRmSolver,'Ntm':PVarNtmSolver}
var_solvers = {'Ntm':PVarNtmSolver}

#which closures to test, also act as suffix
closures = ['None', 'Press', 'Ad']
#closures = ['None']
#which models to use in macro sims.
#vclosures = ['Var', 'TfVar']
vclosures = ['Var']
#initial conditions to examine
ics = [1,2,3]
#ics = [2]
#filename suffixes for each closure
icsuf = {1:'Ic1',2:'Ic2',3:'Ic3'}

#universal (i.e. closure and solver independent) solver params.
gNx = 64
gtNt = 128
gfNt = 32
gdt = 0.1
gcfl = 0.1
gxlo = 0.0
gxhi = 1.0
#style of output, also serves as a suffix
#possible values: {'All', 'Grid', 'None'}
#training
gtio = 'Grid'
#forecasting
gfio = 'Grid' 
#form of training output; whether to output conservative variables ('Cv')
#or primitive variables ('Pv'). 
gtcvo = True
gtcvsuf =  'CvLongTime'

#form of forecasting output; whether to output conservative quantities
#or primitive quantities. 
gfcvo = False
gfcvsuf =  'Pv'


#universal initial condition params
n = 4.0
eps = 1.0

#general var params
gnlags = [2,3,4,5]
gtrain_frac = 0.5
gepochs =  15

#profile plots
make_pplots = False
#state plots
make_stplots = True
#correlation plots
make_cplots = False
#fft plots
make_kplots = False
#spatial stat plots
make_ssplots = False