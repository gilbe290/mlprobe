"""Solvers for simple linear advection in 1d."""
import numpy as np
from numba import jit

@jit
def get_ldqs(ql, qn, qr):
	#returns ql, qr
	if (qr-qn)*(qn-ql)<=0.0:
		return qn, qn
	elif 6.0*(qr-ql)*(qn-0.5*(ql+qr))>(qr-ql)**2:
		return 3.0*qn-2.0*qr, qr
	elif -1*(qr-ql)**2>6.0*(qr-ql)*(qn-0.5*(qr+ql)):
		return ql, 3.0*qn-2.0*ql
	else:
		return ql, qr


@jit
def get_bs(ql,qr):
	#get burgers shock speed
	out = 0.5(ql*ql-qr*qr)/(ql-qr)
	return out


class PExL_pcm(object):
	"""*P*eriodic *Ex*plicit *L*linear piecewise constant method.

	This class is a simple solver for constant linear advection on a uniform
	periodic grid.


	Parameters
	----------
	Nx : int
		Number of spatial grid cells.
	Nt : int
		Number of temporal grid points.
	qinit : function
		Function used to set initial values.
	u : float, optional
		The constant velocity (advection rate) for the system. (default:1.0).
	cfl : float, optional
		Value between 0 and 1.0 specifying the fraction of a cell length that
		can be traversed by waves per timestep. (default:0.7).
	xlo : float, optional.
		Left most x-value of spatial domain. (default:0.0)
	xhi : float, optional.
		Right most x-value of spatial domain. (default:1.0)
	
	Attributes
	----------
	grid : np.ndarray of float64
		Array of approximate solution, has shape (`Nt`+1, `Nx`). After
		initialization, grid[-1] holds the initial conditions.
	ftmp : np.ndarray of float64
		Array of approximate fluxes. 
	dx : float
		Cell widths.
	dt : float
		Time step size.
	u : float
		Advection speed, could also be flux step variable."""

	def __init__(self, Nx, Nt, qinit, u=1.0, cfl=0.7, xlo=0.0, xhi=1.0):
		super()
		self.dx = (xhi-xlo)/Nx
		self.dt = cfl*self.dx/np.abs(u)
		self.u = u
		#extra space cell for pbc 
		self.grid = np.empty((Nt+1, Nx+2), dtype=np.float64)
		self.ftmp = np.empty((Nx+1,), dtype=np.float64)
		xs = np.linspace(0.5*self.dx, (self.grid.shape[1]-2.5)*self.dx,
						num=self.grid.shape[1]-2)
		self.grid[0][1:-1] = qinit(xs)
		self.grid[0][0] = self.grid[0][-2]
		self.grid[0][-1] = self.grid[0][1]
		for i in range(1, self.grid.shape[0]):
			self.step(i)
		self.grid = np.flipud(self.grid)
		self.grid = self.grid[:,1:-1]
	
	def get_x(self):
		return np.linspace(0.5*self.dx, self.grid.shape[1]*self.dx-0.5*self.dx, 
							num=self.grid.shape[1])

	def get_t(self):
		return np.linspace(0, (self.grid.shape[0]-1)*self.dt, num=self.grid.shape[0])
	
	def step(self, i):
		#self.ftmp[i] holds flux inbound from interface at x_{i-1/2}.
		#set fluxes
		if self.u <= 0.0:
			self.ftmp[:] = self.u*self.grid[i-1][1:]
		else:
			self.ftmp[:] = self.u*self.grid[i-1][:-1]
		#update solution
		self.grid[i][1:-1] = self.grid[i-1][1:-1]
		self.grid[i][1:-1] += self.dt*(self.ftmp[:-1]-self.ftmp[1:])/self.dx
		#update bc
		self.grid[i][0] = self.grid[i][-2]
		self.grid[i][-1] = self.grid[i][1]


class PExL_plm(object):
	"""*P*eriodic *Ex*plicit *L*linear piecewise linear method.

	This class is a simple solver for constant linear advection on a uniform
	periodic grid.


	Parameters
	----------
	Nx : int
		Number of spatial grid cells.
	Nt : int
		Number of temporal grid points.
	qinit : function
		Function used to set initial values.
	u : float, optional
		The constant velocity (advection rate) for the system. (default:1.0).
	cfl : float, optional
		Value between 0 and 1.0 specifying the fraction of a cell length that
		can be traversed by waves per timestep. (default:0.7).
	xlo : float, optional.
		Left most x-value of spatial domain. (default:0.0)
	xhi : float, optional.
		Right most x-value of spatial domain. (default:1.0)
	
	Attributes
	----------
	grid : np.ndarray of float64
		Array of approximate solution, has shape (`Nt`+1, `Nx`). After
		initialization, grid[-1] holds the initial conditions.
	ftmp : np.ndarray of float64
		Array of approximate fluxes. 
	dx : float
		Cell widths.
	dt : float
		Time step size.
	u : float
		Advection speed, could also be flux step variable."""

	def __init__(self, Nx, Nt, qinit, u=1.0, cfl=0.7, xlo=0.0, xhi=1.0):
		super()
		self.dx = (xhi-xlo)/Nx
		self.dt = cfl*self.dx/np.abs(u)
		self.u = u
		self.grid = np.empty((Nt+1, Nx+4), dtype=np.float64)
		self.ftmp = np.empty((Nx+1,), dtype=np.float64)
		xs = np.linspace(0.5*self.dx, (self.grid.shape[1]-4.5)*self.dx,
						num=self.grid.shape[1]-4)
		self.grid[0][2:-2] = qinit(xs)
		#set pbc
		self.grid[0][0:2] = self.grid[0][-4:-2]
		self.grid[0][-2:] = self.grid[0][2:4]
		for i in range(1, self.grid.shape[0]):
			self.step(i)
		self.grid = np.flipud(self.grid)
		self.grid = self.grid[:,2:-2]
	
	def get_x(self):
		return np.linspace(0.5*self.dx, self.grid.shape[1]*self.dx-0.5*self.dx, 
							num=self.grid.shape[1])

	def get_t(self):
		return np.linspace(0, (self.grid.shape[0]-1)*self.dt, num=self.grid.shape[0])

	def _mclimiter(self, theta, q):
		"""Monotonized central difference limiter. 

		A function that prevents dispersion and large amounts
		of diffusion in a simple advection equation. 

		Parameters
		----------
		theta : np.ndarray of float64
			Ratio of local and upwind flux.
		q : np.ndarray of float64
			Approximate solution of previous time step.

		Returns
		-------
		out : float
			Limited form of the approximate slope at the interface.
		"""
		#out = np.minimum(2.0, 2.0*theta)
		#out = np.minimum(0.5*(1.0+theta),out)
		#out = np.maximum(0.0, out)
		out = np.fmin(2.0, 2.0*theta)
		out = np.fmin(0.5*(1.0+theta), out)
		out = np.maximum(0.0, out)
		out[2:-1] = out[2:-1]*(q[2:-1]-q[1:-2])
		return out[2:-1]

	def _setf(self, i):
		"""Set the flux values at each interface, based on limited linear
		linear interpolation.
		
		Parameters
		----------
		i : int
			The timestep from which to generate the flux data.
		"""
		#self.ftmp[j] holds flux inbound from interface at x_{j-1/2}.
		#correction terms. Oddly reminiscent of time dependent perturbation
		#in QM. If statement outside so it is only evaluated once.
		#set upwind dQ
		if self.u <= 0.0:
			self.ftmp = self.grid[i-1][3:]-self.grid[i-1][2:-1]
		else:
			self.ftmp = self.grid[i-1][1:-2]-self.grid[i-1][0:-3]
		#set smoothness
		self.grid[i][2:-1] = self.ftmp/(self.grid[i-1][2:-1]-self.grid[i-1][1:-2])
		self.ftmp = self._mclimiter(self.grid[i], self.grid[i-1])
		self.ftmp = 0.5*np.abs(self.u)*(1.0-np.abs(self.u*self.dt/self.dx))*self.ftmp
		#self.ftmp[:] = 0
		#updwinding 
		if self.u <= 0:
			self.ftmp[:] += self.u*self.grid[i-1][2:-1]
		else:
			self.ftmp[:] += self.u*self.grid[i-1][1:-2]
		
	def step(self, i):
		""""""
		#self.ftmp[j] holds flux inbound from interface at x_{j-1/2}.
		#set fluxes
		self._setf(i)
		#update solution
		self.grid[i][2:-2] = self.grid[i-1][2:-2]
		self.grid[i][2:-2] += self.dt*(self.ftmp[:-1] - self.ftmp[1:])/self.dx
		self.grid[i][0:2] = self.grid[i][-4:-2]
		self.grid[i][-2:] = self.grid[i][2:4]


class PExL_ppm(object):
	"""*P*eriodic *Ex*plicit *L*linear piecewise parabolic method.

	This class is a simple solver for constant linear advection on a uniform
	periodic grid.


	Parameters
	----------
	Nx : int
		Number of spatial grid cells.
	Nt : int
		Number of temporal grid points.
	qinit : function
		Function used to set initial values.
	u : float, optional
		The constant velocity (advection rate) for the system. (default:1.0).
	cfl : float, optional
		Value between 0 and 1.0 specifying the fraction of a cell length that
		can be traversed by waves per timestep. (default:0.7).
	xlo : float, optional.
		Left most x-value of spatial domain. (default:0.0)
	xhi : float, optional.
		Right most x-value of spatial domain. (default:1.0)
	
	Attributes
	----------
	grid : np.ndarray of float64
		Array of approximate solution, has shape (`Nt`+1, `Nx`). After
		initialization, grid[-1] holds the initial conditions.
	ftmp : np.ndarray of float64
		Array of approximate fluxes. 
	dx : float
		Cell widths.
	dt : float
		Time step size.
	u : float
		Advection speed, could also be flux step variable."""

	def __init__(self, Nx, Nt, qinit, u=1.0, cfl=0.7, xlo=0.0, xhi=1.0):
		super()
		self.dx = (xhi-xlo)/Nx
		self.dt = cfl*self.dx/np.abs(u)
		self.u = u
		self.grid = np.empty((Nt+1, Nx+6), dtype=np.float64)
		#self.ftmp[2] is the right bound flux out of the interface
		#on the left side of cell self.grid[0][2]
		self.ftmp = np.empty((Nx+4,), dtype=np.float64)
		xs = np.linspace(0.5*self.dx, (self.grid.shape[1]-6.5)*self.dx,
						num=self.grid.shape[1]-6)
		self.grid[0][3:-3] = qinit(xs)
		self.grid[0][0:3] = self.grid[0][-6:-3]
		self.grid[0][-3:] = self.grid[0][3:6]
		for i in range(1, self.grid.shape[0]):
			self.step(i)
		self.grid = np.flipud(self.grid)
		self.grid = self.grid[:,3:-3]
	
	def get_x(self):
		return np.linspace(0.5*self.dx, self.grid.shape[1]*self.dx-0.5*self.dx, 
							num=self.grid.shape[1])

	def get_t(self):
		return np.linspace(0, (self.grid.shape[0]-1)*self.dt, num=self.grid.shape[0])

	def _set_interfaceq(self, i):
		"""Set the values of q at the interface to the right of a cell, based
		on limited piecewise parabolic interpolation of the derivative of
		the primitive function.
		
		Parameters
		----------
		i : int
			The timestep from which the interface values are generated.
			
		Notes
		-----
		This function assumes `self.grid[i+1]` can be overwritten."""

		#set dq.
		self.grid[i+1][:-1] = self.grid[i][1:]-self.grid[i][:-1]
		#set delta_q for grid indexes 1 to N+4
		self.ftmp = 0.5*(self.grid[i][2:]-self.grid[i][:-2])
		#apply limiting
		t0 = 0.0
		t1 = 0.0
		t2 = 0.0
		for j in range(self.ftmp.shape[0]):
			t0 = self.grid[i+1][j] 
			t1 = self.grid[i+1][j+1]
			if t1*t0>0.0:
				t2 = np.abs(self.ftmp[j])
				t0 = 2.0*np.abs(t0)
				t1 = 2.0*np.abs(t1)
				t0 = np.minimum(t0, np.minimum(t1, t2))*np.sign(self.ftmp[j])
				self.ftmp[j] = t0
			else:
				self.ftmp[j] = 0.0
		#compute right side interface values.
		self.grid[i+1][1:-2] = self.grid[i][1:-2] + 0.5*self.grid[i+1][1:-2]
		self.grid[i+1][1:-2] += (self.ftmp[:-1]-self.ftmp[1:])/6.0

	def _setf(self, i):
		"""Set the flux values at each interface, based on limited linear
		linear interpolation.
		
		Parameters
		----------
		i : int
			The timestep for which to generate the flux data.
		"""
		#assemble qright values. 
		self._set_interfaceq(i-1)
		#set ql and qr of each cell.
		#ql
		self.ftmp[1:-1] = self.grid[i][1:-3]
		#qr in self.grid[i][2:-2]
		#form limited ql and qr
		for j in range(2, self.grid.shape[1]-2):
			self.ftmp[j-1], self.grid[i][j] = get_ldqs(self.ftmp[j-1], 
				self.grid[i-1][j], self.grid[i][j])
		#cfl
		c = self.u*self.dt/self.dx 
		dq = 0.0
		q6 = 0.0
		#if statement outside, only evaluated once.
		if self.u <= 0.0:
			for j in range(2, self.ftmp.shape[0]-1):
				dq = self.grid[i][j+1]-self.ftmp[j]
				q6 = 6.0*(self.grid[i-1][j+1]-0.5*(self.grid[i][j+1]+self.ftmp[j]))
				self.ftmp[j] = self.ftmp[j] - 0.5*c*(dq + (1+2.0*c/3.0)*q6)
		else:
			for j in range(2, self.ftmp.shape[0]-1):
				dq = self.grid[i][j]-self.ftmp[j-1]
				q6 = 6.0*(self.grid[i-1][j]-0.5*(self.grid[i][j]+self.ftmp[j-1]))
				self.ftmp[j-2] = self.grid[i][j] - 0.5*c*(dq-(1-2.0*c/3.0)*q6)
			self.ftmp[2:-1] = self.ftmp[:-3]
		self.ftmp[2:-1] = self.u*self.ftmp[2:-1]
	
	def step(self, i):
		""""""
		#self.ftmp[j] holds flux inbound from interface at x_{j-1/2}.
		#set fluxes
		self._setf(i)
		c = self.dt/self.dx
		#update solution
		self.grid[i][3:-3] = self.grid[i-1][3:-3]
		self.grid[i][3:-3] += c*(self.ftmp[2:-2]-self.ftmp[3:-1])
		#update bcs
		self.grid[i][0:3] = self.grid[i][-6:-3]
		self.grid[i][-3:] = self.grid[i][3:6]
