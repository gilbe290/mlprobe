"""Iterates through all test cases specified in `params`."""
import numpy as np
import glob
import tensorflow as tf
tf.keras.backend.set_floatx('float64')
from params import solvers, vsolvers, closures, ics, icsuf, gNx, gfNt, gdt,\
	gcfl, gxlo,gxhi,gfio,gfcvo,gtcvsuf,gfcvsuf,gnlags,gtcvo,vclosures

for sk in solvers.keys():
	for c in closures:
		for i in ics:
			####################
			## Traditional FVM##
			####################
			#base file name of data generated
			fvmfns = 'data/%s%s%s%s*[0-9].npz' % (sk, c, icsuf[i], gtcvsuf)
			fvmlast_out = sorted(glob.glob(fvmfns))[-1]
			#base file name to save forecast
			fvmfns = 'FVMForecast%s%s%s%s' % (sk, c, icsuf[i], gfcvsuf)
			fvm_ic = np.load(fvmlast_out)
			fvm_icn = fvm_ic['n']
			if gtcvo:
				fvm_icu = fvm_ic['nu']/fvm_ic['n']
			else:
				fvm_icu = fvm_ic['u']
			ninit = lambda x:fvm_icn
			uinit = lambda x:fvm_icu
			solvers[sk](gNx, gfNt, gdt, c, ninit, uinit, cfl=gcfl,
						xlo=gxlo, xhi=gxhi, io=gfio, cvout=gfcvo, 
						fn=fvmfns)
			#############
			## VAR FVM ##
			#############
			print("Generating Var forecast.\n")
			for l in gnlags[-3:]:
				print('Using nlags = %d' % l)
				for vc in vclosures:
					sfile = 'data/Restart%d%s%s%s%s.npz' % (l, sk, c, icsuf[i], 
														gtcvsuf)
					mfile = '%sMod%d%s%s%s%s' % (vc, l, sk, c, icsuf[i], 
													gtcvsuf)
					if vc=='Var':
						mfile = '%s.npz' % mfile
						model = np.load(mfile)
						mom_func = lambda x: model['W']@x
					if vc=='TfVar':
						model = tf.keras.models.load_model(mfile)
						mom_func = lambda x: model.predict(np.array([x,],dtype=np.float64), batch_size=1)
					mom_func = lambda x: return x
					varfns = '%sVARForecastMod%d%s%s%s%s' % (vc, l, sk, c, 
															icsuf[i], gfcvsuf)
					vsolvers[sk](gNx, gfNt, gdt, sfile, l, c, mom_func,
									cfl=gcfl, xlo=gxlo, xhi=gxhi, io=gfio,
									cvout=gfcvo, fn=varfns)
