"""Script generating var fits for each test problem."""
import numpy as np
from var import gen_tfvar, gen_var
from params import solvers, closures, ics, icsuf, gNx, gtNt, gtio, gtcvo, gtcvsuf,\
            gnlags, gtrain_frac


for sk in solvers.keys():
    for c in closures:
        for i in ics:
            fnbase = 'data/%s%s%s%s' % (sk, c, icsuf[i], gtcvsuf)
            fnout = '%s%s%s%s' % (sk, c, icsuf[i], gtcvsuf)
            if gtio=='Grid':
                fnbase = '%s%s' % (fnbase, gtio)
            for l in gnlags:
                #print("Getting TF Var")
                #gen_tfvar(fnbase, fnout, l, gtcvo, gNx)
                print("Getting Exact Var")
                gen_var(fnbase, fnout, l, gtcvo, gNx)