"""Basic plotting utilities."""
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as sst


def plot_vspacetime(x, t, vs, labels, fn):
	"""Generate a plot from  arrays in `vs`.

	This function takes a two arrays specifying a spatial and temporal domain,
	and plots each of the associated 2d fields in `vs` as functions of these
	variables and saves the result to disk. 

	Parameters
    ----------
	x : array_like
		A 1d set of spatial points. 
	t : array_like
		A 1d  set of temporal points.
	vs : array_like
		A set of arrays with each satisfying v[i].shape = (t.shape[0],x.shape[0])
	labels : list of str
		Name associated with each field in `vs`.
	fn : str
		Name of file to save figure to. 
	"""
	plt.close('all')
	f, axs = plt.subplots(1,len(vs),sharey=True)
	for i in range(len(vs)):
		dx = 0.5*(x[-1]-x[0])
		dt = 0.5*(t[-1]-t[0])
		#extent = [x[0]-dx, x[-1]+dx, t[0], t[-1]]
		#imi = axs[i].imshow(vs[i], extent=extent)
		#import pdb
		#pdb.set_trace()
		imi = axs[i].imshow(vs[i], aspect='auto', extent=[0, x[-1], 0, t[-1]])
		f.colorbar(imi, ax=axs[i])
		if i == 0:
			axs[i].set_ylabel('t')
			axs[i].set_xlabel('x')
		axs[i].set_title(labels[i])
	plt.savefig(fn)


def plot_corrs(vs, vlabs, bfn):
	nlags = int(vs[0].shape[0]/2.0)
	nvars = len(vlabs)
	ncells = vs[0].shape[1]
	nrp = nvars*ncells
	#join all  values
	vnet = np.zeros((vs[0].shape[0], nvars*ncells), dtype=np.float64)
	for i in range(nvars):
		lb = ncells*i
		ub = lb + ncells
		vnet[:,lb:ub] = vs[i][:,:]
	#get means and stddevs of each column 
	vtmeans = np.mean(vnet, axis=0)
	vtdevs = np.std(vnet, axis=0)
	#store all at once for nice colorbar 
	rmat = np.zeros((nlags, nrp, nrp), dtype=np.float64)
	for k in range(nlags):
		tu = vnet.shape[0]-k
		for i in range(vnet.shape[1]):
			for j in range(vnet.shape[1]):
				rmat[k,i,j] = np.mean((vnet[k:,i]-vtmeans[i])*(vnet[:tu, j]-vtmeans[j]))
				rmat[k,i,j] = rmat[k,i,j]/(vtdevs[i]*vtdevs[j])
	nmax = np.maximum(np.abs(np.amin(rmat)), np.abs(np.amax(rmat)))
	#plot
	for k in range(nlags):
		fn = "%s_%04d.png" % (bfn, k)
		t = 'lag = %d' % k
		plt.close('all')
		m = plt.matshow(rmat[k,:,:], cmap='RdBu', vmin=-1*nmax, vmax=nmax)
		plt.colorbar(m)
		for i in range(1, nvars):
			lab = '%s' % (vlabs[i-1])
			of = ncells*i
			plt.vlines(of, 0, nrp, label=lab)
			plt.hlines(of, 0, nrp, label=lab)
		plt.title(t)
		plt.savefig(fn)


def plot_space_stats(t, vs, labels, s, fn):
	"""Given a list of 2d arrays, plot the cumulants of each elements'
	columns as a function of their row number. 
	
	Parameters
	----------
	t : np.ndarray
		The set of times corresponding to each row of observations.
	vs : list of np.ndarray
		A list of 2d arrays, with columns representing space values.
	labels : list of str
		Labels for each element of vs.
	s : str
		Name of quantity stats are examining.
	fn : str
		File name for generated plot.
	"""
	#generate states
	slabels = ['mean', 'vari', 'skew', 'kurt']
	svals = {'mean':1, 'vari':2,'skew':3,'kurt':4}
	sdict = {'mean':[], 'vari':[],'skew':[],'kurt':[]}
	for i in range(len(vs)):
		sidict = {'mean':[], 'vari':[], 'skew':[],'kurt':[]}
		for j in range(vs[i].shape[0]):
			for k in sidict.keys():
				sidict[k].append(sst.kstat(vs[i][j,:], n=svals[k]))
		for k in sidict.keys():
			sdict[k].append(sidict[k])
	plt.close('all')
	fig, axs = plt.subplots(4, 1, sharex=True, squeeze=False)
	for i in range(len(slabels)):
		axs[i,0].set_ylabel(slabels[i])
		for j in range(len(labels)):
			c = 'C%d' % j
			if i==0:
				axs[i,0].plot(t, sdict[slabels[i]][j], c=c, label=labels[j])
			else:
				axs[i,0].plot(t, sdict[slabels[i]][j], c=c)
		if i==0:
			axs[i,0].legend()
		if i==3:
			axs[i,0].set_xlabel('t')
	fig.suptitle('%s Spatial Stats' % s)
	plt.savefig(fn)