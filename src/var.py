"""Methods for generating constrained vector auto-regressions."""
import glob
import numpy as np
import scipy.linalg as sla
import statsmodels.api as sm
import tensorflow as tf
tf.keras.backend.set_floatx('float64')
from params import gepochs, gtrain_frac


def gen_tfdata(fbase, nlags, cvo, Nx):
    """Given a lag number and base file, generate a tensor flow dataset from
    output file names and the `nlags` file names before them."""
    fstr_fmt = '%s*.npz' % fbase
    flist = sorted(glob.glob(fstr_fmt))
    Xout = []
    Yout = []
    Muout = []
    Xtmp = np.empty((nlags, Nx, 2), dtype=np.float64)
    #Xtmp = np.empty((nlags, Nx), dtype=np.float64)
    Ytmp = np.empty((Nx,), dtype=np.float64)
    for i in range(nlags, len(flist)):
        ydat = np.load(flist[i])
        if cvo:
            Ytmp[:] = ydat['nu']
        else:
            Ytmp[:] = ydat['u']*ydat['n']
        Yout.append(np.copy(Ytmp))
        for j in range(1, nlags+1):
            xjdat = np.load(flist[i-j])
            Xtmp[j-1, :, 0] = xjdat['n']
            if cvo:
                Xtmp[j-1,:,1] = xjdat['nu']
            else:
                Xtmp[j-1,:,1] = xjdat['u']
        Xout.append(np.copy(Xtmp).flatten())
    out = tf.data.Dataset.from_tensor_slices((Xout, Yout))
    return out


def gen_tfvar(fbase, fout, nlags, cvo, Nx):
    """Generate Var based off tensor flow sgd like iteration."""
    of = 'TfVarMod%d%s' % (nlags, fout)
    dat = gen_tfdata(fbase, nlags, cvo, Nx) 
    var_model = tf.keras.Sequential()
    var_model.add(tf.keras.layers.Dense(Nx, use_bias=False))
    var_model.compile(optimizer=tf.keras.optimizers.RMSprop(), 
                    loss=tf.keras.losses.MeanSquaredError(), metrics=['accuracy','mse'])
    var_model.fit(dat.batch(20), epochs=gepochs, verbose=2)
    var_model.save(of)


def generate_data(fbase, nlags, cvo, Nx):
    """Given a lag number and base file, generate a data matrix and 
    output matrix from file names and the `nlags` file names before them."""
    fstr_fmt = '%s*[0-9].npz' % fbase
    flist = sorted(glob.glob(fstr_fmt))
    Xout = []
    Yout = []
    Xtmp = np.empty((nlags, Nx, 2), dtype=np.float64)
    #Xtmp = np.empty((nlags, Nx), dtype=np.float64)
    Ytmp = np.empty((Nx,), dtype=np.float64)
    for i in range(nlags, len(flist)):
        ydat = np.load(flist[i])
        if cvo:
            Ytmp[:] = ydat['nu']
        else:
            Ytmp[:] = ydat['u']
        Yout.append(np.copy(Ytmp))
        for j in range(1, nlags+1):
            xjdat = np.load(flist[i-j])
            Xtmp[j-1, :, 0] = xjdat['n']
            if cvo:
                Xtmp[j-1,:,1] = xjdat['nu']
            else:
                Xtmp[j-1,:,1] = xjdat['u']
        Xout.append(np.copy(Xtmp).flatten())
    #Yout.shape = (nobservations, n_nodes)
    Yout = np.array(Yout)
    #Xout.shape = (nobservations, n_lags*n_nodes*2)
    Xout = np.array(Xout)
    return Xout, Yout


def check_stable(W, nl, nx, ny, nv):
    """Takes the update matrix `W` associated with a VAR(`nl`) process and
    constructs the companion matrix for the corresponding VAR(1) process to
    check that the forecast will remain stable.
    
    Parameters
    ----------
    W : np.ndarray
        Update matrix for the process.
    nl : int
        The number of lags used for the process.
    nx : int
        The number of nodes used in each lag.
    ny : int
        Number of outputs from update.
    nv : int
        Number of variables associated with each node in a process."""
    Cmat = np.zeros((ny+nv*nl*nx, ny+nv*nl*nx), dtype=np.float64)


def gen_var(fbase, fout, nlags, cvo, Nx):
    """Given a lt of observations of unkowns and knowns, generate the matrix
    of all observations up to a maximum lag used to generate auto-regression."""
    X, Y = generate_data(fbase, nlags, cvo, Nx)
    mu = np.mean(X, axis=0)
    Xc = X-mu
    mod = sm.tsa.VAR(X)
    omod = mod.fit(15)
    print(omod.is_stable())
    import pdb
    pdb.set_trace()
    #W = np.empty((Nx, X.shape[1]))
    #Wi = sla.lstsq(X, Y, cond=1e-2)
    #print('Residues for %d lags:\n' % nlags)
    #for i in W[1]:
    #    print(i)
    #print('Singular Values for %d lags:\n' % nlags)
    #for i in W[-1]:
    #    print(i)
    #for i in Wi[-1]:
    #    print(i) 
    #of = 'VarMod%d%s' % (nlags, fout)
    #np.savez(of, W=Wi[0])