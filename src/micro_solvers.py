"""Base classes for fvm solutions to isothermal euler equations
with unspecified eos."""
import numpy as np

#uber-pedantic note: is it more reasonable to
#specify a domain and then set of equations,
#or set of equations and then a domain?

class PExBaseSolver(object):
    """*P*eriodic *Ex*plicit solver base class.
    
    This class is a skeleton for 1st and 2nd order solvers of the 1d
    isothermal Euler equations with unspecified equation of state.
    
    Parameters
    ----------
    o : int
        Order of solver being used. 
    Nx : int
        Number of spatial grid cells.
    Nt : int
        Number of time samples to store beyond given initial condition.
    dt : float
        Time interval between spatial solutions stored in `grid`.
    closure : {'Press', 'Ad', 'None'}
        How the right hand side of the momentum equation is fixed.
    ninit : function
        Function used to set initial number density values.
    uinit : function
        Function used to set initial velocity values.
    cfl : float, optional
        Value between 0 and 1.0 specifying the fraction of a cell length that
        can be traversed by waves per timestep. (default:0.7)
    xlo : float, optional
        Left most x-value of spatial domain. (default:0.0)
    xhi : float, optional
        Right most x-value of spatial domain. (default:1.0)
    io : {'None', 'All', 'Grid', 'Both'}, optional
        Specifies how to output data. 'All' writes out every single timestep,
        including subcycles. 'Grid' only writes out the final grid. For custom
        file names, add a 'fn' kwarg. 
    cvout : boolean, optional
        Determines if the conserved variables or independent fields are written
        to disk. 

        
    Attributes
    ----------
    o : int
        Solver order.
    nsteps : int
        Total number of steps taken.
    io : str, {'None', 'All', 'Grid'}
        Output specification.
    closure : str, {'Press', 'Ad', 'None'}
        Closure being used on the system.
    base_fn : str
        Base file name for outputs.
    dx : float
        Cell widths.
    dt : float
        Temporal spacing of solutions in `grid`.
    grid : np.ndarray of float64
        Array for approximate solution, has shape ('Nt'+1, 'Nx')
    stmp : np.ndarray of float64
        Array for temporary storage of approximate solution, used in e.g.
        sub-cycling.
    phi : np.ndarray of complex64
        An array for a potential, only created if `closure` holds value of
        'Ad'.
    ftmp : np.ndarray of float64
        Array of approximate fluxes.
    """

    def __init__(self, o, Nx, Nt, dt, closure, ninit, uinit, cfl=0.7, 
                    xlo=0.0, xhi=1.0, io='None', cvout=True, **kwargs):
        super().__init__()
        self.o = o
        self.nsteps = 0
        self.io = io
        self.closure = closure
        if 'fn' in kwargs.keys():
            self.base_fn = kwargs['fn']
        else: 
            self.base_fn = 'DD'
        self.cvout = cvout
        self.dx = (xhi-xlo)/Nx
        self.dt = dt
        self.cfl = cfl
        #extra space cell on each end for pbc
        self.grid = np.zeros((Nt+1, Nx+2*o, 2), dtype=np.float64)
        self.stmp = np.empty_like(self.grid[0])
        #create potential array if needed.
        if self.closure == 'Ad':
            self.phi = np.empty((Nx+2*o), dtype=np.complex64)
        self.ftmp = np.empty((Nx+1, 2), dtype=np.float64)
        #set initial conditions
        xs = np.linspace(0.5*self.dx, (self.grid.shape[1]-2*o-0.5)*self.dx,
                            num=self.grid.shape[1]-2*o)
        self.grid[0, o:-1*o, 0] = ninit(xs)
        self.grid[0,o:-1*o,1] = uinit(xs)
        self.grid[0,o:-1*o,1] = self.grid[0,o:-1*o,0]*self.grid[0,o:-1*o,1]
        #set pbc
        self.grid[0,:o] = self.grid[0,-2*o:-1*o]
        self.grid[0,-1*o:] = self.grid[0, o:2*o]
        #put initial conditions in stmp as well
        self.stmp[:,:] = self.grid[0,:,:]
        #save initial conditions
        if self.io=='All' or self.io=='Both':
            fn = "%s%06d" % (self.base_fn, self.nsteps)
            self._save_step(fn, self.dt, self.grid[0,self.o:-1*self.o,:])
    
    def get_x(self):
        return np.linspace(0.5*self.dx, self.grid.shape[1]*self.dx-0.5*self.dx,
                            num=self.grid.shape[1])

    def get_t(self):
        return np.linspace(0, (self.grid.shape[0]-1)*self.dt, 
                             num=self.grid.shape[0])

    def _solve_splitop(self, i, dt):
        """For systems with self.closure of value `Ad`, solve the poisson 
        equation and update the momentum equation."""
		#split operator
        self.grid[i,:,0] = self.stmp[:,0]
		#solve for potential
        k = np.fft.rfftfreq(self.phi.shape[0]-2*self.o, d=self.dx)
        self.phi[:k.shape[0]] = np.fft.rfft(self.stmp[self.o:-1*self.o,0])
        #multiply next line by -1 to make force repulsive.
        self.phi[1:k.shape[0]] = self.phi[1:k.shape[0]]/(4.0*np.pi*np.pi*k[1:]**2)
        self.phi[self.o:-1*self.o] = np.fft.irfft(self.phi[:k.shape[0]])
		#pbc
        self.phi[:self.o] = self.phi[-2*self.o:-1*self.o]
        self.phi[-1*self.o:] = self.phi[self.o:2*self.o]
		#update solution. 
        a = dt/(2.0*self.dx)
        #set force
        #note: analytic form for indexing wanted here.
        if self.o == 1:
            self.grid[i,1:-1,1] = -1*a*(self.phi[2:]-self.phi[:-2])
        elif self.o ==2:
            self.grid[i,2:-2,1] = -1*a*(self.phi[3:-1]-self.phi[1:-3])
		#forward euler with gradient
        self.grid[i,self.o:-1*self.o,1] = self.grid[i,self.o:-1*self.o,1]\
            *self.stmp[self.o:-1*self.o,0]
        self.grid[i,self.o:-1*self.o,1] += self.stmp[self.o:-1*self.o,1]
		#pbc
        self.grid[i,:self.o,:] = self.grid[i,-2*self.o:-1*self.o,:]
        self.grid[i,-1*self.o:,:] = self.grid[i,self.o:2*self.o,:]
        self.stmp[:,:] = self.grid[i,:,:]

    def _find_dt(self, i, tleft):
        """Determine the maximum local wave speeds of the system, store them
        for later use, and then use the global max to generate largest stable
        timestep for a fixed cfl."""
        #store bulk flow velocity.
        self.grid[i,:,0] = np.divide(self.stmp[:,1], self.stmp[:,0],
                                     out=np.zeros_like(self.stmp[:,0]),
                                     where=self.stmp[:,0]!=0.0)
        umax = 0.0
        out_dt = 0.0
        if self.closure == 'Press':
            #eval 1
            self.grid[i,:,1] = np.abs(self.grid[i,:,0]-1.0)
            #eval 0
            self.grid[i,:,0] = np.abs(self.grid[i,:,0]+1.0)
            #max evals
            self.grid[i,:,0] = np.maximum(self.grid[i,:,0], self.grid[i,:,1])
        else:
            self.grid[i,:,0] = np.abs(self.grid[i,:,0])
        #global max
        umax = np.amax(self.grid[i,:,0])
        if self.dt*umax/self.dx <= self.cfl:
            out_dt = self.dt
        else:
            out_dt = self.cfl*self.dx/umax
        #the above ensures out_dt holds a stable timestep.
        #now make sure we only step to the time value we want.
        if tleft - out_dt <= 0.0:
            out_dt = tleft
        return out_dt

    def _save_step(self, fn, dt, sarray):
        """Save a solution time step step `sarray` and the time step used 
        to generate it to disk on file `fn`"""
        if self.cvout:
            np.savez(fn, n=sarray[:,0], nu=sarray[:,1], dt=[dt])
        else:
            u = np.divide(sarray[:,1], sarray[:,0], 
                            out=np.zeros_like(sarray[:,0]),
                            where=sarray[:,0]!=0.0)
            np.savez(fn, n=sarray[:,0], u=u, dt=[dt])



class PExRmSolver(PExBaseSolver):
    """*P*eriodic *Ex*plicit *R*usanov *M*ethod solver.
    
    This class is a first order solver for the 1d isothermal Euler equations
    with one of three possible equations of state.
    
    Parameters
    ----------
    o : int
        Order of solver being used. 
    Nx : int
        Number of spatial grid cells.
    Nt : int
        Number of time samples to store beyond given initial condition.
    dt : float
        Temporal spacing between solutions in `grid`.
    closure : {'Press', 'Ad', 'None'}
        How the right hand side of the momentum equation is fixed.
    ninit : function
        Function used to set initial number density values.
    uinit : function
        Function used to set initial velocity values.
    cfl : float, optional
        Value between 0 and 1.0 specifying the fraction of a cell length that
        can be traversed by waves per timestep. (default:0.7)
    xlo : float, optional
        Left most x-value of spatial domain. (default:0.0)
    xhi : float, optional
        Right most x-value of spatial domain. (default:1.0)
    io : {'None', 'All', 'Grid'}, optional
        Specifies how to output data. 'All' writes out every single timestep,
        including subcycles. 'Grid' only writes out the final grid. For custom
        file names, add a 'fn' kwarg. 
    cvout : boolean, optional
        Determines if the conserved variables or independent fields are written
        to disk. 

        
    Attributes
    ----------
    o : int
        Solver order.
    nsteps : int
        Total number of steps taken.
    io : str, {'None', 'All', 'Grid'}
        Output specification.
    closure : str, {'Press', 'Ad', 'None'}
        Closure being used on the system.
    base_fn : str
        Base file name for outputs.
    dx : float
        Cell widths.
    dt : float
        Temporal spacing of solutions in `grid`, initially 0.
    grid : np.ndarray of float64
        Array for approximate solution, has shape ('Nt'+1, 'Nx')
    stmp : np.ndarray of float64
        Array for temporary storage of approximate solution, used in e.g.
        sub-cycling.
    ftmp : np.ndarray of float64
        Array of approximate fluxes.
    """

    def __init__(self, Nx, Nt, dt, closure, ninit, uinit, cfl=0.7, 
                    xlo=0.0, xhi=1.0, io='None', cvout=True, **kwargs):
        super().__init__(1, Nx, Nt, dt, closure, ninit, uinit, cfl=cfl, 
                            xlo=xlo, xhi=xhi, io=io, cvout=cvout, **kwargs)
        #generate data
        for i in range(1, self.grid.shape[0]):
            self.step(i)
		#flip so initial condition is at highest time index
        self.grid = np.flipud(self.grid)
		#eliminate ghost cells
        self.grid = self.grid[:,1:-1, :]
		#generate field variables
        if not self.cvout:
            self.grid[:,:,1] = np.divide(self.grid[:,:,1], self.grid[:,:,0],
								out=np.zeros_like(self.grid[:,:,1]), 
								where=self.grid[:,:,0]!=0.0)
        #save grid if desired. 
        if self.io=='Grid' or self.io=='Both':
            ofn = '%sGrid' % self.base_fn
            if self.cvout:
                np.savez(ofn, n=self.grid[:,:,0], 
								nu=self.grid[:,:,1])
            else:
                np.savez(ofn, n=self.grid[:,:,0], 
                                u=self.grid[:,:,1])
	
    def _setf(self, i):
        """Generate fluxes for the system based off of rusanov method.
		
		This function generates stable fluxes by adding artificial viscosity
		based on the maximum eigen value of the flux jacobian at each
		interface, which is stored in `self.grid[i,:,0]`.

		Parameters
		----------
		i : int
			Grid index holding wave speeds.

		Notes
		-----
		This method assumes that `self.stmp` holds the *conserved* values.
		"""
		#set viscosity term
        self.ftmp[:,0] = np.maximum(self.grid[i,:-1,0], self.grid[i, 1:,0])
        self.ftmp[:,1] = self.ftmp[:,0]
		#set f0
        self.ftmp[:, 0] = -1*self.ftmp[:,0]*(self.stmp[1:,0]-self.stmp[:-1,0])
        self.ftmp[:, 0] += self.stmp[:-1, 1]
        self.ftmp[:,0] += self.stmp[1:,1]
		#set f1
        self.ftmp[:, 1] = -1*self.ftmp[:,1]*(self.stmp[1:,1]-self.stmp[:-1,1])
		#use self.grid[i,:,1] to generate possible nan fluxes. Sacrificing code clarity
		#and simplicity for vectorization and minimal memory use. Caching is bad though.
        self.grid[i,:,1] = self.stmp[:,1]**2
        self.grid[i,:,1] = np.divide(self.grid[i,:,1], self.stmp[:,0],
							out=np.zeros_like(self.grid[i,:,1]), 
                            where=self.stmp[:,0]!=0.0)
        #possible pressure term
        if self.closure == 'Press':
            self.grid[i,:,1] += self.stmp[:,0]
        self.ftmp[:,1] += self.grid[i,:-1,1] + self.grid[i,1:,1]
        self.ftmp[:,:] = 0.5*self.ftmp[:,:]

    def step(self, i):
        """Generate and store the approximate solution across the spatial 
		domain at time step i.
		
		Parameters
		----------
		i : int
			The index of the sub-array of `self.grid` to put new data in.
		"""
		#local t info. Could re-factor so dt is used in _find_dt,
        #decreasing mem use.
        tleft = self.dt
        dt = tleft
        p = 0.0
        while tleft != 0.0:
            self.nsteps += 1
            dt = self._find_dt(i, tleft)
            self._setf(i)
            self.grid[i,1:-1,:] = self.stmp[1:-1, :]
            self.grid[i,1:-1,:] += dt*(self.ftmp[:-1,:]-self.ftmp[1:,:])/self.dx
			#update bc
            self.grid[i,0,:] = self.grid[i,-2,:]
            self.grid[i,-1,:] = self.grid[i,1,:]
			#store new result
            self.stmp[:,:] = self.grid[i,:,:]
            if self.closure == 'Ad':
                self._solve_splitop(i,dt)
            if self.io == 'All' or self.io=='Both':
                fn = '%s%06d' % (self.base_fn, self.nsteps)
                self._save_step(fn, dt, self.stmp[1:-1,:])
            tleft -= dt
            p = 1.0 - tleft/self.dt
            print("Subcycling. %f percent done with time step." % p)



class PExNtmSolver(PExBaseSolver):
    """ *P*eriodic *Ex*plicit *N*essanyaho *T*admor method solver.
    
    This class is a second order solver for the 1d isothermal Euler equations
    with one of three possible equations of state.
    
    Parameters
    ----------
    o : int
        Order of solver being used. 
    Nx : int
        Number of spatial grid cells.
    Nt : int
        Number of time samples to store beyond given initial condition.
    dt : float
        Time step between the spatial solutions stored in `grid`.
    closure : {'Press', 'Ad', 'None'}
        How the right hand side of the momentum equation is fixed.
    ninit : function
        Function used to set initial number density values.
    uinit : function
        Function used to set initial velocity values.
    cfl : float, optional
        Value between 0 and 1.0 specifying the fraction of a cell length that
        can be traversed by waves per timestep. (default:0.7)
    xlo : float, optional
        Left most x-value of spatial domain. (default:0.0)
    xhi : float, optional
        Right most x-value of spatial domain. (default:1.0)
    io : {'None', 'All', 'Grid'}, optional
        Specifies how to output data. 'All' writes out every single timestep,
        including subcycles. 'Grid' only writes out the final grid. For custom
        file names, add a 'fn' kwarg. 
    cvout : boolean, optional
        Determines if the conserved variables or independent fields are written
        to disk. 

    Attributes
    ----------
    o : int
        Solver order.
    nsteps : int
        Total number of steps taken.
    io : str, {'None', 'All', 'Grid'}
        Output specification.
    closure : str, {'Press', 'Ad', 'None'}
        Closure being used on the system.
    base_fn : str
        Base file name for outputs.
    dx : float
        Cell widths.
    dt : float
        Temporal spacing of solutions in `grid`, initially 0.
    grid : np.ndarray of float64
        Array for approximate solution, has shape ('Nt'+1, 'Nx')
    stmp : np.ndarray of float64
        Array for temporary storage of approximate solution, used in e.g.
        sub-cycling.
    ftmp : np.ndarray of float64
        Array of approximate fluxes.
    """
    def __init__(self, Nx, Nt, dt, closure, ninit, uinit, cfl=0.7, 
                    xlo=0.0, xhi=1.0, io='None', cvout=True, **kwargs):
        super().__init__(2, Nx, Nt, dt, closure, ninit, uinit, cfl=cfl, 
                            xlo=xlo, xhi=xhi, io=io, cvout=cvout, **kwargs)
        self.t = 1.5
        #generate data
        for i in range(1, self.grid.shape[0]):
            self.step(i)
		#flip so initial condition is at highest time index
        self.grid = np.flipud(self.grid)
		#eliminate ghost cells
        self.grid = self.grid[:,2:-2, :]
		#generate field variables
        if not self.cvout:
            self.grid[:,:,1] = np.divide(self.grid[:,:,1], self.grid[:,:,0],
								out=np.zeros_like(self.grid[:,:,1]), 
								where=self.grid[:,:,0]!=0.0)
        #save grid if desired. 
        if self.io=='Grid' or self.io=='Both':
            ofn = '%sGrid' % self.base_fn
            if self.cvout:
                np.savez(ofn, n=self.grid[:,:,0], 
								nu=self.grid[:,:,1])
            else:
                np.savez(ofn, n=self.grid[:,:,0], u=self.grid[:,:,1])
    
    def _set_sig(self, i):
		#store unlimited slopes
        self.grid[i,1:,:] = self.stmp[1:,:]-self.stmp[:-1,:]
		#set phi
        self.grid[i,1:-1,:] = self.grid[i, 2:,:]/self.grid[i, 1:-1,:]
		#limit, can be switched to minmod if desired.
        for j in range(self.grid.shape[1]-2):
			#minmod like
            self.grid[i, j, :] = np.fmin(self.t*self.grid[i,j+1,:],
				                	0.5*(1 + self.grid[i,j+1,:]))
            self.grid[i, j, :] = np.fmin(self.t, self.grid[i,j,:])
            self.grid[i,j,:] = np.maximum(0, self.grid[i,j,:])
			#sweby
			#r=self.grid[i,j+1,:]
			#self.grid[i,j,:] = np.fmin(r, self.t)
			#self.grid[i,j,:] = np.maximum(np.fmin(self.t*r, 1.0), self.grid[i,j,:])
			#self.grid[i,j,:] = np.maximum(0, self.grid[i,j,:])
			#minmod
			#self.grid[i,j,:] = np.maximum(0, np.fmin(1.0, self.grid[i,j+1,:]))
		#set sigmas
		#note since we only use 0.5*dx*sigma, don't divide by dx here.
        self.grid[i,1:-1,:] = 0.5*self.grid[i,:-2,:]*(self.stmp[1:-1,:]
                                -self.stmp[:-2,:])

    def _setf1(self, i, dt):
        """Generate fluxes for the system based off of Nessyahu-Tadmor method.
		
		This function generates stable fluxes by adding artificial viscosity
		based on the maximum eigen value of the flux jacobian at each
		interface, which is stored in `self.grid[i,:,0]`.

		Parameters
		----------
		i : int
			Grid index holding reconstruction slopes.
		dt : float
			Time step size.

		Notes
		-----
		This method assumes that `self.stmp` holds the *conserved* values.
		"""
        qm = np.zeros((self.grid.shape[-1],), dtype=np.float64)
        qp = np.zeros((self.grid.shape[-1],), dtype=np.float64)
        ahi = 0.0
        alo = 0.0
        for j in range(self.ftmp.shape[0]):
            qm = self.stmp[j+1,:] + self.grid[i,j+1,:]
            qp = self.stmp[j+2,:] - self.grid[i,j+2,:]
            ahi = qp[1]/qp[0]
            alo = qm[1]/qm[0]
            if self.closure=='Press':
                ahi = np.fmax(np.abs(ahi-1.0), np.abs(ahi+1.0))
                alo = np.fmax(np.abs(alo-1.0), np.abs(alo+1.0))
            else:
                ahi = np.abs(ahi)
                alo = np.abs(alo)
            ahi = np.fmax(ahi, alo)
            self.ftmp[j,:] = -1.0*ahi*(qp-qm)
            #set f0
            self.ftmp[j,0] += qm[1] + qp[1]
            #set f1
            self.ftmp[j,1] += ((qm[1]**2)/qm[0])
            self.ftmp[j,1] += ((qp[1]**2)/qp[0])
            #f1 possible pressure term
            if self.closure=='Press':
                self.ftmp[j, 1] += qm[0] + qp[0]
        self.ftmp[:,:] = 0.5*dt*self.ftmp[:,:]/self.dx
			
    def _setf2(self, i, dt):
        qm = np.zeros((self.grid.shape[-1],), dtype=np.float64)
        qp = np.zeros((self.grid.shape[-1],), dtype=np.float64)
        ahi = 0.0
        alo = 0.0
        self.ftmp[:,:] = -1.0*self.ftmp[:,:]
        for j in range(self.ftmp.shape[0]):
            qm = self.stmp[j+1,:] + self.grid[i,j+1,:]
            qp = self.stmp[j+2,:] - self.grid[i,j+2,:]
            ahi = qp[1]/qp[0]
            alo = qm[1]/qm[0]
            if self.closure=='Press':
                ahi = np.fmax(np.abs(ahi-1.0), np.abs(ahi+1.0))
                alo = np.fmax(np.abs(alo-1.0), np.abs(alo+1.0))
            else:
                ahi = np.abs(ahi)
                alo = np.abs(alo)
            ahi = np.fmax(ahi, alo)
            self.ftmp[j,:] += -0.5*ahi*dt*(qp-qm)/self.dx
            #set f0
            self.ftmp[j,0] += 0.5*dt*(qm[1] + qp[1])/self.dx
            #set f1
            self.ftmp[j,1] += 0.5*dt*((qm[1]**2)/qm[0])/self.dx
            self.ftmp[j,1] += 0.5*dt*((qp[1]**2)/qp[0])/self.dx
            #possible pressure
            if self.closure=='Press':
                self.ftmp[j,1] += 0.5*dt*(qm[0] + qp[1])/self.dx
        self.ftmp[:,:] = 0.5*self.ftmp[:,:]

    def step(self, i):
        """Generate and store the approximate solution across the spatial 
		domain at time step i.
		
		Parameters
		----------
		i : int
			The index of the sub-array of `self.grid` to put new data in.
		"""
		#local t info. Could re-factor so dt is used in _find_dt,
        #decreasing mem use.
        tleft = self.dt
        dt = tleft
        p = 0.0
        while tleft != 0.0:
            self.nsteps += 1
            dt = self._find_dt(i, tleft)
            #for ad do strang split
            if self.closure=='Ad':
                dt = 0.5*dt
            #ad closure split is hyperbolic-ad-hyperbolic
            #ssp rk2
            #first step (ad: step 1, operator 1)
            self._set_sig(i)
            self._setf1(i, dt)
            self.stmp[2:-2,:] += (self.ftmp[:-1,:]-self.ftmp[1:,:]) 
            #pbc
            self.stmp[:2,:] = self.stmp[-4:-2,:]
            self.stmp[-2:,:] = self.stmp[2:4,:]
            #step 2 (ad: step 2, operator 1)
            self._set_sig(i)
            self._setf2(i, dt)
            self.grid[i,2:-2,:] = self.stmp[2:-2,:]
            self.grid[i,2:-2,:] += (self.ftmp[:-1,:]-self.ftmp[1:,:])
            #pbc
            self.grid[i,:2,:] = self.grid[i,-4:-2,:]
            self.grid[i,-2:,:] = self.grid[i,2:4,:]
			#store new result
            self.stmp[:,:] = self.grid[i,:,:]
            if self.closure == 'Ad':
                self._solve_splitop(i, 2.0*dt)
                #operator 1, step 1
                self._set_sig(i)
                self._setf1(i, dt)
                self.stmp[2:-2,:] += (self.ftmp[:-1,:]-self.ftmp[1:,:])
                #pbc
                self.stmp[:2,:] = self.stmp[-4:-2,:]
                self.stmp[-2:, :] = self.stmp[2:4,:]
                #operator 1, step 2
                self._set_sig(i)
                self._setf2(i, dt)
                self.grid[i, 2:-2,:] = self.stmp[2:-2,:]
                self.grid[i,2:-2,:] += (self.ftmp[:-1,:]-self.ftmp[1:,:])
                #pbc
                self.grid[i,:2,:] = self.grid[i,-4:-2,:]
                self.grid[i,-2:,:] = self.grid[i,2:4,:]
                #store new result
                self.stmp[:,:] = self.grid[i,:,:]
                dt = 2.0*dt
            if self.io == 'All' or self.io=='Both':
                fn = '%s%06d' % (self.base_fn, self.nsteps)
                self._save_step(fn, dt, self.stmp[2:-2,:])
            tleft -= dt
            p = 1.0 - tleft/self.dt
            print("Subcycling. %f percent done with time step." % p)