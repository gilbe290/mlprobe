import numpy as np
from params import n, eps

#ic1: wave density, wave velocity
def nic1(x):
    return np.sin(n*np.pi*x) + eps

def uic1(x):
    return np.sin(n*np.pi*x)

#ic2: wave density, small velocity
def nic2(x):
    return np.sin(n*np.pi*x) + eps

def uic2(x):
    return eps*np.ones_like(x)

#ic3: small density, wave velocity
def nic3(x):
    return eps*np.ones_like(x)

def uic3(x):
    return np.sin(n*np.pi*x)


def gen_sol(sclass, Nx, Nt, dt, closure, ic, cfl, xlo, xhi, 
			io, cvo, fns):
	"""Run a solver of type sclass, based on specified ic.
	
	Parameters
	----------
	sclass : class
		Solver to use, any sub-class of `PExBase`.
	Nx : int
		Number of spatial cells.
	Nt : int
		Number of time steps to store.
	dt : float
		Spacing between time steps.
	closure : {'None', 'Press', 'Ad'}
		Which eos to close physical model.
	ic : int
		Which set of initial conditions to start from.
	xlo : float
		Left most position of domain.
	xhi : float
		Right most position of domain.
	cfl : float
		Value in [0,1] specifying fraction of a cell length
		that can be traversed by waves per timestep.
	io : {'None','Grid','All'}
		Output style.
	cvo : bool
		Format of output.
	fns : str
		Base file name for outputs.
	"""
	if ic == 1:
		solv = sclass(Nx, Nt, dt, closure, nic1, uic1, cfl=cfl,
					xlo=xlo, xhi=xhi, io=io, cvout=cvo, fn=fns)
	elif ic == 2:
		solv = sclass(Nx, Nt, dt, closure, nic2, uic2, cfl=cfl,
					xlo=xlo, xhi=xhi, io=io, cvout=cvo, fn=fns)
	elif ic == 3:
		solv = sclass(Nx, Nt, dt, closure, nic3, uic3, cfl=cfl,
					xlo=xlo, xhi=xhi, io=io, cvout=cvo, fn=fns)
	del(solv)


def gen_hmm_sol(sclass, s, Nx, Nt, dt, closure, ic, cfl, xlo, xhi, 
				io, cvo, fns):
	"""Run a solver of type sclass, based on specified ic.
	
	Parameters
	----------
	sclass : class
		Solver to use, any sub-class of `PExBase`.
	Nx : int
		Number of spatial cells.
	Nt : int
		Number of time steps to store.
	dt : float
		Spacing between time steps.
	closure : {'None', 'Press', 'Ad'}
		Which eos to close physical model.
	ic : int
		Which set of initial conditions to start from.
	xlo : float
		Left most position of domain.
	xhi : float
		Right most position of domain.
	cfl : float
		Value in [0,1] specifying fraction of a cell length
		that can be traversed by waves per timestep.
	io : {'None','Grid','All'}
		Output style.
	cvo : bool
		Format of output.
	fns : str
		Base file name for outputs.
	"""
	if ic == 1:
		solv = sclass(s, Nx, Nt, dt, closure, nic1, uic1, cfl=cfl,
					xlo=xlo, xhi=xhi, io=io, cvout=cvo, fn=fns)
	elif ic == 2:
		solv = sclass(s, Nx, Nt, dt, closure, nic2, uic2, cfl=cfl,
					xlo=xlo, xhi=xhi, io=io, cvout=cvo, fn=fns)
	elif ic == 3:
		solv = sclass(s, Nx, Nt, dt, closure, nic3, uic3, cfl=cfl,
					xlo=xlo, xhi=xhi, io=io, cvout=cvo, fn=fns)
	del(solv)